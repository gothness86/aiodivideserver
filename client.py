import sys
from os.path import dirname

sys.path.append(dirname(__file__))

from client.application import Client

if __name__ == '__main__':

    client = Client()

    try:
        client.run()
    except KeyboardInterrupt:
        print("Client stopped")
    finally:
        client.stop()
