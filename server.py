import sys
from os.path import dirname

sys.path.append(dirname(__file__))

from server.application import Server
from server.actions.primes import Primes


if __name__ == '__main__':

    server = Server(action=Primes())

    try:
        print("Server is running")
        server.run()
    except InterruptedError:
        print("Server stopped")
    finally:
        server.stop()
