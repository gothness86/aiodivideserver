from abc import ABCMeta, abstractmethod


class AbstractActions(metaclass=ABCMeta):

    @abstractmethod
    def run(self, data):
        pass
