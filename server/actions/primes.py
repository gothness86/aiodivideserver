import asyncio

from server.actions.abstract import AbstractActions
from server.settings import MAX_VALUE


class Primes(AbstractActions):

    __eratosthenes = [0, 0]

    def __init__(self):
        """
        Построение списка простых чисел методом
        решета Эратосфена
        :return:
        """

        length = int(MAX_VALUE / 2) + 1

        if len(self.__eratosthenes) < length:
            self.__eratosthenes += list(range(2, length))

            for i in range(2, len(self.__eratosthenes)):
                for j in range(i + i, len(self.__eratosthenes), i):
                    self.__eratosthenes[j] = 0

            # Очистка от пустых значений
            self.__eratosthenes = [x for x in self.__eratosthenes if x != 0]

    @asyncio.coroutine
    def division(self, value):
        """
        Рекурсивное разделение на простые множители.
        TODO: Нужно рассмотреть другие алгоритмы решения задачи
        :param value: Число которое необходимо разделить
        :return:
        """

        if value == 1:
            return []

        for i in self.__eratosthenes:
            if value % i == 0:
                data = yield from self.division(int(value / i))
                return [i, ] + data

        return [value, ]

    @asyncio.coroutine
    def run(self, data):
        """
        Проверка числа и запуск разделения на простые множители
        :param data: Число которое необходимо разделить
        :return: Возвращает список из простых чисел или False
        """

        if data > MAX_VALUE or data <= 2:
            return False

        result = yield from self.division(data)

        return result
