import asyncio
import concurrent.futures

from server.actions.abstract import AbstractActions
from server.settings import HOST, PORT, TIMEOUT


class Server(object):
    """
    AsyncIO Socket Server
    """

    __action = None

    @property
    def action(self):
        return self.__action

    @action.setter
    def action(self, data):
        if isinstance(data, AbstractActions):
            self.__action = data
        else:
            raise ValueError("ERROR: Action is not valid")

    def __init__(self, action, host=HOST, port=PORT):
        """
        Конструктор с инициализацией сервера
        :param host:
        :param port:
        """
        self.__loop = asyncio.get_event_loop()
        self.__server = asyncio.start_server(
            self.handle_client,
            host=host, port=port
        )
        self.action = action

    def run(self):
        """
        Запуск сервера
        :return:
        """
        self.__loop.run_until_complete(self.__server)
        self.__loop.run_forever()

    def stop(self):
        """
        Остановка сервера
        :return:
        """
        self.__server.close()
        self.__loop.close()

    @asyncio.coroutine
    def handle_client(self, reader, writer):
        """
        Обработка запросов клиентов
        :param reader:
        :param writer:
        :return:
        """

        while not reader.at_eof():

            try:
                data = yield from asyncio.wait_for(reader.readline(), timeout=TIMEOUT)
            except (concurrent.futures.TimeoutError, ConnectionResetError):
                break

            try:
                data = int(str(data.decode()))
            except (ValueError, TypeError, UnicodeDecodeError):
                writer.write("ERROR: Value incorrect\r\n".encode())
                continue

            result = yield from self.action.run(data)

            if result is not False:
                writer.write((str(result) + "\r\n").encode())
            else:
                writer.write("ERROR: Value incorrect\r\n".encode())

        writer.close()
