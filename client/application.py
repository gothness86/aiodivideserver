import sys
import threading
import socket
from time import sleep
from random import randint

from client.settings import HOST, PORT


class Client(object):

    def __init__(self):
        """
        Создание сокета и потоков
        """
        self.__connected = False
        self.__socket = socket.socket()
        self.__reader = threading.Thread(target=self.reader, args=())
        self.__writer = threading.Thread(target=self.writer, args=(), daemon=True)

    def run(self, host=HOST, port=PORT):
        """
        Подключение к серверу и запуск потоков
        :param host:
        :param port:
        :return:
        """

        try:
            self.__socket.connect((host, port))
        except socket.error as msg:
            print("ERROR: ", msg, file=sys.stderr)
            return False
        else:
            self.__connected = True

        self.__writer.start()
        self.__reader.start()

        while self.__connected:
            sleep(3)

    def stop(self):
        """
        Закрытие сокета
        :return:
        """
        self.__socket.close()

    def reader(self):
        """
        Чтение данных из сокета и вывод на экран
        :return:
        """
        while True:

            try:
                data = self.__socket.recv(1024)
            except ConnectionAbortedError:
                break

            if not data:
                print("ERROR: Connection closed", file=sys.stderr)
                break

            print(data.decode())

        self.__connected = False

    def writer(self):
        """
        Отправка данных на сервер
        :return:
        """
        while True:
            # value = str(randint(1000, 100000)) + '\r\n'
            value = sys.stdin.readline()

            if not self.__connected:
                break

            try:
                self.__socket.send(value.encode())
            except ConnectionAbortedError:
                break
